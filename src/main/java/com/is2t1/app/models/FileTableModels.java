/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author JADPA26
 */
public class FileTableModels extends AbstractTableModel {
      private String[][] data;
    private final Object columnContacto[] = {"Casa", "Trabajo", "Movil"};

    public FileTableModels() {
        data = new String[][]{
            {null, null}
        };
    }

    public FileTableModels(String f[][]) {
        data = f;
    }

    public FileTableModels(File f) {
        File files[] = f.listFiles();
        String[][] xdata = new String[files.length][3];
        int c = 0;
        for (File file : files) {
            try {
                xdata[c][0] = file.getName();
                xdata[c][1] = file.getAbsolutePath();
                BasicFileAttributes attr = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                xdata[c][0] = file.getName();
            } catch (IOException ex) {
                Logger.getLogger(FileTableModels.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        setDataModel(xdata);
    }

    public void setDataModel(String f[][]) {
        data = f;
    }

    @Override
    public int getRowCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
}
