/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.controllers;

import com.is2t1.app.models.Contact;
import com.is2t1.app.views.ContactFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author JADPA26
 */
public class Guardado implements ActionListener, FocusListener {

     private ContactFrame contactframe;
     private JFileChooser dialog;
     
     public Guardado(ContactFrame pf ){
         contactframe = pf ;
         dialog = new JFileChooser();
         
         
         
     }
     
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            
          
            case "Guardar":
                save();
                 break;
            
             case "clear":
                clear();
                break;
            case "select":
                select();
                break;           
        }
        
    }

    @Override
    public void focusGained(FocusEvent arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
       private void save() {
        dialog.showSaveDialog(contactframe);
        if (dialog.getSelectedFile() != null) {
            writeFile(dialog.getSelectedFile(), contactframe.getData());
        }

     }

    private void clear() {
        contactframe.clear();
    }

    private void select() {
        dialog.showOpenDialog(contactframe);
        if (dialog.getSelectedFile() != null) {
            Contact p = readFile(dialog.getSelectedFile());
            contactframe.showData(p);
        }
    }

    private Contact readFile(File selectedFile) {
       Contact Contacto = null;
        try {
           String file = null;
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            Contacto = (Contact) in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Contact.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Contact.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Contacto;
    }
    }

